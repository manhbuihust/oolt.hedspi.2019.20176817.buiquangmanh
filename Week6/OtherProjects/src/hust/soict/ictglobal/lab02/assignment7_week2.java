package hust.soict.ictglobal.lab02;

import java.util.Scanner;


public class assignment7_week2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number row of matrix: ");
		int r = sc.nextInt();
		System.out.println("Enter the number column of matrix: ");
		int c = sc.nextInt();
		
		int i, j;
		int arr1[][] = new int[r][c];
		int arr2[][] = new int[r][c];
		int result[][] = new int[r][c];
				
		System.out.println("Enter elements of the first matrix: ");	
		for(i = 0; i < r; i++)
		{
			for(j = 0; j < c; j++)
			{
				arr1[i][j] = sc.nextInt();
				
			}
		}
			
		
		
		System.out.println("Enter elements of the second matrix: ");	
		for(i = 0; i < r; i++)
		{
			for(j = 0; j < c; j++)
			{
				arr2[i][j] = sc.nextInt();
				result[i][j] = arr1[i][j] + arr2[i][j];
					
			}		
		}
		
		System.out.println("Matrix 1: ");
		for(i = 0; i < r; i++)
		{
			for(j = 0; j < c; j++)
			{
				System.out.print(arr1[i][j] + "\t");
			}
			System.out.println();
		}	
		
		System.out.println("Matrix 2: ");
		for(i = 0; i < r; i++)
		{
			for(j = 0; j < c; j++)
			{
				System.out.print(arr2[i][j] + "\t");
			}
			System.out.println();
		}	
		
		
		// print result
		System.out.println("Sum of matrix: ");
		for(i = 0; i < r; i++)
		{
			for(j = 0; j < c; j++)
			{
				System.out.print(result[i][j] + "\t");
			}
			System.out.println();
		}
		
		System.exit(0);
		
	}
}
