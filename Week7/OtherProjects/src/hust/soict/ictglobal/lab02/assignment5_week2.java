package hust.soict.ictglobal.lab02;

import javax.swing.JOptionPane;

public class assignment5_week2 {
	public static void main(String[] args) {
		String strDay, strMonth, strYear;
		boolean check = true;
		int day, month, year;
		
		do {
			strDay = JOptionPane.showInputDialog(null, "Please enter day: ", "Program display day/month/year", JOptionPane.INFORMATION_MESSAGE);
			day = Integer.parseInt(strDay);
			if(day < 0 || day > 30)
			{
				JOptionPane.showInputDialog(null, "The day incompatible. Please enter day again", "Program display day/month/year", JOptionPane.INFORMATION_MESSAGE);
				check = false;
			}
		}while(!check);
		
		check = true;
		do {
			strMonth = JOptionPane.showInputDialog(null, "Please enter month: ", "Program display day/month/year", JOptionPane.INFORMATION_MESSAGE);
			month = Integer.parseInt(strMonth);
			if(month < 0 || month > 12)
			{
				JOptionPane.showInputDialog(null, "The month incompatible. Please enter month again", "Program display day/month/year", JOptionPane.INFORMATION_MESSAGE);
				check = false;
			}
		}while(!check);
		
		
		check = true;
		do {
			strYear = JOptionPane.showInputDialog(null, "Please enter year: ", "Program display day/month/year", JOptionPane.INFORMATION_MESSAGE);
			year = Integer.parseInt(strYear);
			if(year < 0)
			{
				JOptionPane.showInputDialog(null, "The year incompatible. Please enter year again", "Program display day/month/year", JOptionPane.INFORMATION_MESSAGE);
				check = false;
			}
		}while(!check);
		
		
		String strNotification = Integer.toString(day) + "/" + Integer.toString(month) + "/" + Integer.toString(year);
		JOptionPane.showMessageDialog(null, strNotification, "Program display day/month/year", JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
}
