package hust.soict.ictglobal.aims.disc;

public class DigitalVideoDisc {
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;
	
	
	public DigitalVideoDisc(String title) {
		super();
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	
	public void printOrder()
	{
		System.out.println("DVD - " + title + " - " + category + " - " + director + " - " + length + ": " + cost + "$");
	}
	
	public boolean search(String title)
	{
		int check = 0;
		String split_title[] = title.split(" ", 0);
            
		
		for(String ti: split_title)
		{
			if(this.title.contains(ti))
			{
				check += 1;
			}
			else
			{
				break;
			}
		}
		
		if(check == split_title.length)
		{
			return true;
		}
		
		return false;
	}
}


