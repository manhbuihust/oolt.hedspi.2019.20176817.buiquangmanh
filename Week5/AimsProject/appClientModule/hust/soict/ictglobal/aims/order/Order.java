package hust.soict.ictglobal.aims.order;
import hust.soict.ictglobal.aims.disc.DigitalVideoDisc;
import java.lang.Math; 

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	
	private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private static int qtyOrdered;
	private static String dateOrdered;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	
	public void setQtyOrdered(int qtyOrdered_input) {
		qtyOrdered = qtyOrdered_input;
	}
	
	public int addDigitalVideoDisc(DigitalVideoDisc disc)
	{	
		int check = this.checkFull();
		if(check == 1)
		{
			System.out.println("The order is almost full");
			return 1;
		}
			
		
		itemOrdered[qtyOrdered] = disc;
		qtyOrdered += 1;
		System.out.println("The disc has been added");
		
		return 0;
	}
	
	public int addDigitalVideoDisc(DigitalVideoDisc [] disc)
	{
		int i, check;
		
		for(i = 0; i < disc.length; i++)
		{
			check = this.addDigitalVideoDisc(disc[i]);
			if(check == 1)
			{
				System.out.println("The order is almost full");
				return 1;
			}
			qtyOrdered += 1;
			System.out.println("The disc has been added");
		}
		
		
		return 0;
	}
	
	public static String getDateOrdered() {
		return dateOrdered;
	}

	public Order() {
		super();
		qtyOrdered = 0;
		Order.nbOrders += 1;
	}

	public void setDateOrdered(String dateOrdered) {
		Order.dateOrdered = dateOrdered;
	}


	public int addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2)
	{
		int check;
		
		check = this.addDigitalVideoDisc(dvd1);
		if(check == 1)
		{
			System.out.println("The dvd1 cant be added");
			return 1;
		}
		else
		{
			System.out.println("The dvd1 has been added");
		}
		
		check = this.addDigitalVideoDisc(dvd2);
		if(check == 1)
		{
			System.out.println("The dvd2 cant be added");
			return 1;
		}
		else
		{
			System.out.println("The dvd2 has been added");
		}
		
		return 0;
	}
	
	public int checkFull()
	{
		if(qtyOrdered == MAX_NUMBERS_ORDERED)
		{
			return 1;
		}
		
		return 0;
	}
	
	public int removeDigitalVideoDisc(DigitalVideoDisc disc)
	{
		int check = 0; // not found item
		if(qtyOrdered == 0)
		{
			System.out.println("Dont remove");
			return 1;
		}
		
		int i = 0;
		for(i = 0; i < qtyOrdered; i++)
		{
			if(this.itemOrdered[i] == disc)
			{
				check = 1;
				this.itemOrdered[i] = this.itemOrdered[i+1];
				break;
			}
		}
		
		if(check == 1)
		{
			qtyOrdered -= 1;
			for(int j = i + 1; j < qtyOrdered; j++)
			{
				this.itemOrdered[j] = this.itemOrdered[j + 1];
			}
		}
		else
		{
			System.out.println("Items not found");
		}
		
		return 0;
	}
	
	public float totalCost()
	{	
		float sum = 0;
		for(int i = 0; i < qtyOrdered; i++)
		{  	
			sum += itemOrdered[i].getCost();
		}
		
		return sum;
	}
	
	
	public void printListOrders()
	{
		System.out.println("*********************Order************************");
		System.out.println("Date: " + dateOrdered);
		System.out.println("Ordered Items: ");
		
		for(int i = 0; i < qtyOrdered; i++)
		{
			itemOrdered[i].printOrder();
		}
		
		System.out.println("Total cost: " + this.totalCost());
	}
	
	public DigitalVideoDisc[] getItemOrdered() {
		return itemOrdered;
	}

	public void setItemOrdered(DigitalVideoDisc[] itemOrdered) {
		this.itemOrdered = itemOrdered;
	}

	public DigitalVideoDisc getALuckyItem()
	{
		int lucky_number;
		
		do {
			double rand = Math.random();
			lucky_number = (int)rand;
		}while(lucky_number < 0 || lucky_number >= qtyOrdered);
		
		
		DigitalVideoDisc lucky_item =  this.itemOrdered[lucky_number];
		this.removeDigitalVideoDisc(lucky_item);
		
		return lucky_item;
	}
}

