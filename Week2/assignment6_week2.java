import java.util.Arrays;

public class assignment6_week2 {

	public static void main(String[] args) {
		int[] arr = new int[] {11, 41, 32, 35, 31, 13, 15, 19, 30};
		int sum = 0;
		double avg = 0;
		
		for(int i: arr)
		{
			sum += i;
		}
		
		avg = sum / arr.length;
		
		System.out.println("Original arrays: ");
		System.out.println(Arrays.toString(arr));
		Arrays.sort(arr);
		System.out.println("Sorted array: ");
		System.out.println(Arrays.toString(arr));
		System.out.println("Sum of array: " + sum);
		System.out.println("Avg of array: " + avg);
		
		System.exit(0);
	}

}
