import java.util.*;

public class assignment4_week2 {
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		int height;
		int i, j, k;
		
		System.out.print("Enter height of the triangle: ");
		height = sc.nextInt();
		
		for(i=0; i < height + 1; i++)
		{
			for(j=height; j > i; j--)
			{
				System.out.print(" ");
			}
			
			for(k = 0; k < (2 * i - 1); k++)
			{
				System.out.print("*");
			}
			
			System.out.println();
		}
	}
}
