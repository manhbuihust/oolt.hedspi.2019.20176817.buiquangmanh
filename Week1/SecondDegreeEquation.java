import javax.swing.JOptionPane;
import java.lang.Math; 

public class SecondDegreeEquation {
	public static void main(String[] args)
	{
		String strA, strB, strC;
		String strNotification = "Enter second degree equation one variable";
		double x = 0.0;
		
		JOptionPane.showMessageDialog(null, strNotification, "Solve first degree equation one variable", JOptionPane.INFORMATION_MESSAGE);
		
		strA = JOptionPane.showInputDialog(null, "Please input a: ", "Solve first degree equation one variable", JOptionPane.INFORMATION_MESSAGE);
		double a = Double.parseDouble(strA);
				
		strB = JOptionPane.showInputDialog(null, "Please input a: ", "Solve first degree equation one variable", JOptionPane.INFORMATION_MESSAGE);
		double b = Double.parseDouble(strB);
		
		strC = JOptionPane.showInputDialog(null, "Please input a: ", "Solve first degree equation one variable", JOptionPane.INFORMATION_MESSAGE);
		double c = Double.parseDouble(strC);
		
		double delta = Math.pow(b, 2) - 4 * a * c;
		
		if (delta < 0)
		{
			strNotification = "Phuong trinh vo nghiem";
		}
		else
		{
			String x1 = Double.toString((-b + Math.pow(delta, 0.5)) / (2 * a) );
			String x2 = Double.toString((-b - Math.pow(delta, 0.5)) / (2 * a) );
			strNotification = "x1 = " + x1 + "    x2 = " + x2;
		}
		
		JOptionPane.showMessageDialog(null, strNotification, "Result", JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
}
