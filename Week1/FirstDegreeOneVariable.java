import javax.swing.*;

public class FirstDegreeOneVariable {
	public static void main(String[] args)
	{
		String strA, strB;
		String strNotification = "Enter first degree equation one variable";
		double x = 0.0;
		
		JOptionPane.showMessageDialog(null, strNotification, "Solve first degree equation one variable", JOptionPane.INFORMATION_MESSAGE);
		
		strA = JOptionPane.showInputDialog(null, "Please input a: ", "Solve first degree equation one variable", JOptionPane.INFORMATION_MESSAGE);
		double a = Double.parseDouble(strA);
				
		strB = JOptionPane.showInputDialog(null, "Please input a: ", "Solve first degree equation one variable", JOptionPane.INFORMATION_MESSAGE);
		double b = Double.parseDouble(strB);
		
		if(a == 0)
		{
			x = -b;
			strNotification = Double.toString(x);
		}
		else
		{
			strNotification = Double.toString(-b / a);
		}
		
		JOptionPane.showMessageDialog(null, strNotification, "Result", JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
}
