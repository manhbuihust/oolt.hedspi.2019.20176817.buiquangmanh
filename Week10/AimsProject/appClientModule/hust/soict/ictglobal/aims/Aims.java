package hust.soict.ictglobal.aims;
import java.util.*;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.*;

public class Aims {
	public static void main(String[] args) {
		java.util.Collection<DigitalVideoDisc> collection = new java.util.ArrayList<DigitalVideoDisc>();
		Scanner sc = new Scanner(System.in);
		int exit = 1;
		
		do {
			System.out.print("Enter title: ");
			String title = sc.nextLine();
			System.out.print("Enter the lenght of dvd: ");
			int length = Integer.parseInt(sc.nextLine());
			System.out.print("Enter cost: ");
			float cost = Float.parseFloat(sc.nextLine());
			
			System.out.println("\n-------------------------------------------");
			DigitalVideoDisc dvd = new DigitalVideoDisc();
			dvd.setTitle(title);
			dvd.setCost(cost);
			dvd.setLength(length);
			collection.add(dvd);
			System.out.print("Enter 0 to exit or 1 to continue: ");
			exit = Integer.parseInt(sc.nextLine());
		}while(exit != 0);
		
		// iterate through the array list and output their titles
		java.util.Iterator<DigitalVideoDisc> iterator = collection.iterator();
		
		System.out.println("\n-----------------------------------------");
		System.out.println("The DVDs currently in the order are: ");
		
		while(iterator.hasNext())
		{
			System.out.println(((Media)iterator.next()).getTitle());
		}
		
		// Sort the collection of DVDs - based on the compareTo()
		java.util.Collections.sort((java.util.List<DigitalVideoDisc>)collection);
		
		iterator = collection.iterator();
		System.out.println("\n-----------------------------------------");
		System.out.println("The DVDs in sorted order are: ");
		
		while(iterator.hasNext())
		{
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		
		System.out.println("-----------------------------------------");
	}

}

