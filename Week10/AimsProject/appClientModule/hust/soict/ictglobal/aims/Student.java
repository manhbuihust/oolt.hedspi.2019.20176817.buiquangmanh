package hust.soict.ictglobal.aims;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Student {
	private String studentID;
	private String studentNameString;
	private Double gpa;
	private String birthdayDate;
	
	public Student(String id, String name, String day, Double mark) throws IllegaBirthDayException, IllegalGPAException {
		this.setStudentID(id);
		this.setStudentNameString(name);
		
		if (isValidDate(day)) {
			System.out.println("ERR: INVALID FORMAT DATE");
			throw(new IllegaBirthDayException());
		}
		
		if (mark < 0 || mark > 4) {
			System.out.println("ERR: INVALID GPA ");
			throw(new IllegalGPAException());
		}
		this.setBirthdayDate(birthdayDate);
		
		this.setGpa(mark);
	}
	
	public static boolean isValidDate(String d)
	{
		String regex = "^(1[0-2]|0[1-9])/(3[01]"
                + "|[12][0-9]|0[1-9])/[0-9]{4}$"; 
		Pattern pattern = Pattern.compile(regex); 
		Matcher matcher = pattern.matcher((CharSequence)d); 
		return matcher.matches(); 
	}
	
	public String getStudentID() {
		return studentID;
	}

	public void setStudentID(String studentID) {
		this.studentID = studentID;
	}

	public String getStudentNameString() {
		return studentNameString;
	}

	public void setStudentNameString(String studentNameString) {
		this.studentNameString = studentNameString;
	}

	public Double getGpa() {
		return gpa;
	}

	public void setGpa(Double gpa) {
		this.gpa = gpa;
	}

	public String getBirthdayDate() {
		return birthdayDate;
	}

	public void setBirthdayDate(String birthdayDate) {
		this.birthdayDate = birthdayDate;
	}
}
