package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.PlayerException;

public interface Playable {
	public void play() throws PlayerException;
	
}
