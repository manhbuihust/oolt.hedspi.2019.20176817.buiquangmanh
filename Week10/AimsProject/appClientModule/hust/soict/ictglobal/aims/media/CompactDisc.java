package hust.soict.ictglobal.aims.media;
import java.util.ArrayList;

import hust.soict.ictglobal.aims.PlayerException;


public class CompactDisc extends Disc implements Playable, Comparable<CompactDisc> {
	private String artist;
	private int length;
	ArrayList<Track> tracks = new ArrayList<Track>();
	
	public CompactDisc(String title) {
		super(title);
	}
	
	public CompactDisc()
	{
		
	}
	
	public String getArtist() {
		return artist;
	}
	
	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	public void addTrack(Track track)
	{
		if(this.tracks.contains(track))
		{
			System.out.println("Already exist in the list of tracks");
		}
		else
		{
			this.tracks.add(track);
			System.out.println("The track has been added");
		}
	}
	
	public void removeTrack(Track track)
	{
		if(this.tracks.contains(track))
		{
			this.tracks.remove(track);
			System.out.println("The trach has been deleted");
		}
		else
		{
			System.out.println("Dont exist in the list of tracks");
		}
	}
	
	public void removeTrack(int i)
	{
		this.tracks.remove(i);
		System.out.println("The trach has been deleted");
	}
	
	public int getLength()
	{
		int l = 0;
		
		for(Track e: tracks)
		{
			l += e.getLength();
		}
		
		return l;
	}

	@Override
	public void play() throws PlayerException {
		if (this.getLength() <= 0) {
			System.err.println("ERR: CD length is 0");
			throw(new PlayerException());
		}
		
		System.out.println("Playing CD: " + this.getTitle());
		System.out.println("CD length: " + this.getLength());
		System.out.println("---------PLAY THE LIST OF TRACKS----------");
		
		java.util.Iterator iter = tracks.iterator();
		Track nextTrack;
		
		while(iter.hasNext())
		{
			nextTrack = (Track) iter.next();
			try {
				nextTrack.play();
			} catch (PlayerException e) {
				e.printStackTrace();
			}
		}
		
	}



	@Override
	public int compareTo(CompactDisc o) {
		int result = this.getTitle().compareTo(o.getTitle());
		
		return result;
	}
}
