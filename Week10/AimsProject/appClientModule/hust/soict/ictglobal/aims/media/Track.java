package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.PlayerException;

public class Track implements Playable, Comparable<Track>{
	private String title;
	private int length;
	
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setLength(int lenght) {
		this.length = lenght;
	}

	@Override
	public void play() throws PlayerException{
		if (this.getLength() <= 0) {
			System.err.println("ERROR: DVD length is 0");
			throw(new PlayerException());
		}
		
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

	@Override
	public int compareTo(Track o) {
		int result = this.getTitle().compareTo(o.getTitle());
		
		return result;
	}
	
	
}
