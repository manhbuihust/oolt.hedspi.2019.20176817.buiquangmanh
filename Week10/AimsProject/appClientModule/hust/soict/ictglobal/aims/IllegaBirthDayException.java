package hust.soict.ictglobal.aims;

public class IllegaBirthDayException extends Exception {

	public IllegaBirthDayException() {
		// TODO Auto-generated constructor stub
	}

	public IllegaBirthDayException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public IllegaBirthDayException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public IllegaBirthDayException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public IllegaBirthDayException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
