package hust.soict.ictglobal.gui.awt;
import java.awt.*; // using awt container and component classes
import java.awt.event.*; // Using awt event classes and listener interfaces

// an awt program inherits from the top-level container java.awt.Frame
public class AWTCounter extends Frame implements ActionListener {
	private Label lblCount; // Declare a Label component
	private TextField tfCount; // Declare a TextField component
	private Button btnCount; //Declare a Button component
	private int count = 0; // Counter's value
	
	
	// Constructor to setup GUI components and event handlers
	public AWTCounter() {
		setLayout(new FlowLayout());
		// "super" Frame, which is a container, sets its layout to FlowLayout to arrange
		// the components-from-left-to-right, and flow to next row from top-to-bottom
		
		lblCount = new Label("Counter"); // construct the label component
		add(lblCount); // "super" Frame container adds Label component
		
		tfCount = new TextField(count + "", 10); // construct the TextField component with initial text
		tfCount.setEditable(false); // set to read-only
		add(tfCount); // "super" frame container adds TextField component
		
		btnCount = new Button("Count"); // construct the Button component
		add(btnCount); // "super" Frame container adds Button component
		
		btnCount.addActionListener(this);
		setTitle("AWT Counter"); // "super" Frame sets its title
		setSize(250, 100); // "super" Frame sets its initial window size
		
		setVisible(true); // "super" Frame shows
	}
	
	// The entry main() method
	public static void main(String[] args)
	{
		AWTCounter app = new AWTCounter();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		++count; // Increase the counter value
		// Display the counter value on the TextField tfCount
		tfCount.setText(count + ""); // convert int to String
	}
	
	
}
