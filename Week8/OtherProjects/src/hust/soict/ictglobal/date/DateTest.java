package hust.soict.ictglobal.date;

import java.util.Date;
import java.util.Scanner;

public class DateTest {
	public static void main(String[] args) {
		Scanner sc =new Scanner (System.in);
        String str[];
        int i,n;                               
        
        System.out.print("Enter no. of dates");
        n = sc.nextInt();
        str= new String[n];
        
        sc.nextLine();                              
        System.out.println("Enter date: ");
        for(i = 0; i < n; i++)
        {                                           
            str[i] = sc.nextLine();                               //getting date as string from user
        }
        
		DateUtils.compareTwoDates(str[0], str[1]); // compare
		Date date[] = DateUtils.sort(str); // sort
		
		for(i = 0; i < date.length; i++)
		{
			System.out.println(DateUtils.sdformat.format(date[i]));
		}
		
		
		
		
	}

}
