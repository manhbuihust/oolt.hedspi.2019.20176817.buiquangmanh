package hust.soict.ictglobal.aims.media;
import java.util.*;
import java.util.stream.Collectors;

public class Book extends Media implements Comparable<Book> {
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrquency;
	
	public Book()
	{
		
	}
	
	public Book(String title) {
		super(title);
	}

	public Book (String title, String category) {
		super(title, category);
	}
	
	public Book (String title, String category, List<String> authors)
	{
		super(title, category);
		this.authors = authors;
	}
	
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	public void setAuthors(String author)
	{
		this.addAuthor(author);
	}
	
	public void addAuthor(String authorName)
	{
		if(this.authors.indexOf(authorName) == -1)
		{
			this.authors.add(authorName);
		}
		else
		{
			System.out.println("The author name has been exist");
		}
	}
	
	public void removeAuthor(String authorName)
	{
		int index = this.authors.indexOf(authorName);
		if(index != -1)
		{
			this.authors.remove(index);
			System.out.println("The author name has been removed");
		}
		else
		{
			System.out.println("The author name dont exist");
		}
	}

	@Override
	public int compareTo(Book o) {
		int result = this.getTitle().compareTo(o.getTitle());
		
		return result;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		this.processContent();
	}

	public String toString() 
	{	
		String tokenList = "";
		String frequencyList = "";
		int i = 0;
		

		java.util.Iterator<String> iterator = this.contentTokens.iterator();
		while(iterator.hasNext())
		{
			if(i%10 == 0)
				tokenList = tokenList + "\n";
			
			tokenList = tokenList + "  " + iterator.next();
			i++;
		}
		tokenList = tokenList + "\n";
		
		for(Map.Entry<String, Integer> entry: this.wordFrquency.entrySet())
		{
			frequencyList += entry.getKey() + ": " + Integer.toString(entry.getValue()) + "\n";
		}
		
		return "------------------------------------------------\n" +
			   "Title: " + this.getTitle() + "\n" +
			   "Category: " + this.getCategory() + "\n" +
			   "Authors: " + this.authors.toString() + "\n" + 
			   "Cost: " + Float.toString(this.getCost()) + "\n" +
			   "Original Content: " + this.content + "\n" +
			   "The content lenght: " + Integer.toString(this.contentTokens.size()) + "\n" +
			   "The token list: "  + "\n" + 
			   tokenList + 
			   "The word frequency: " + "\n" +
			   frequencyList;
		
	}
	
	public void processContent()
	{
		String regex = "[!_,'@? ]";
		
		// split the content to tokens by space or punctuations
		java.util.StringTokenizer str = new StringTokenizer(this.content, regex);
		
		// save to the contentTokens
		while(str.hasMoreTokens())
		{
			this.contentTokens.add(str.nextToken().toString());
		}
		
		// sort a->z
		Collections.sort(this.contentTokens);
		
		// count the frequency of each token
		this.wordFrquency = new java.util.HashMap<String, Integer>();
		java.util.Iterator<String> iterator = this.contentTokens.iterator();
		
		while(iterator.hasNext())
		{
			String key = iterator.next();
			if(this.wordFrquency.containsKey(key)) // update
			{
				
				int value = this.wordFrquency.get(key) + 1;
				this.wordFrquency.remove(key);
				this.wordFrquency.put(key, value);
			}
			else
			{
				this.wordFrquency.put(key, 1); 
			}
		}	
	}
}
