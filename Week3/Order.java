public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	
	private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered;
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public int addDigitalVideoDisc(DigitalVideoDisc disc)
	{	
		if(this.qtyOrdered == MAX_NUMBERS_ORDERED)
		{
			System.out.println("The order is almost full");
			return 1;
		}
		else
		{
			this.itemOrdered[this.qtyOrdered] = disc;
			this.qtyOrdered += 1;
			System.out.println("The disc has been added");
		}
		return 0;
	}
	
	public int removeDigitalVideoDisc(DigitalVideoDisc disc)
	{
		if(this.qtyOrdered == 0)
		{
			System.out.println("Dont remove");
			return 1;
		}
		
		int i = 0;
		for(i = 0; i < this.itemOrdered.length; i++)
		{
			if(this.itemOrdered[i] == disc)
			{
				this.itemOrdered[i] = this.itemOrdered[i+1];
				i++;
				break;
			}
		}
		
		for(int j = i + 1; j < this.itemOrdered.length; j++)
		{
			this.itemOrdered[j] = this.itemOrdered[j + 1];
		}
		
		return 0;
	}
	
	public float totalCost()
	{	
		float sum = 0;
		for(int i = 0; i < this.qtyOrdered; i++)
		{  	
			sum += itemOrdered[i].getCost();
		}
		
		return sum;
	}
	
}
